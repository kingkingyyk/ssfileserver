FROM python:3.9-alpine

ADD server.py requirements.txt /app/
ADD templates /app/templates

WORKDIR app

RUN pip install --no-cache-dir -r requirements.txt &&\
    rm -f requirements.txt

CMD ["gunicorn", "--workers", "5", "--bind", "0.0.0.0:17723", "server:app"]