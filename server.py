from flask import Flask, send_from_directory, request, Response, render_template
import humanize
import os

app = Flask(__name__, template_folder='templates')
ROOT_PATH = os.environ.get('ROOT_DIR', r'D:\Documents\King')

@app.route('/browse')
def browse():
    global ROOT_PATH
    path = request.args.get('path', '')
    root_path_sep = ROOT_PATH + os.path.sep
    full_path = os.path.abspath(os.path.join(ROOT_PATH, path)) if path else root_path_sep
    if full_path.startswith(root_path_sep) and os.path.exists(full_path) and os.path.isdir(full_path):
        folders = [{
            'name': x,
            'path': os.path.join(path, x),
        } for x in os.listdir(full_path) if os.path.isdir(os.path.join(full_path, x))]
        files = [{
            'name': x,
            'path': os.path.join(path, x),
            'size': humanize.naturalsize(os.path.getsize(os.path.join(full_path, x))),
        } for x in os.listdir(full_path) if os.path.isfile(os.path.join(full_path, x))]
        ppath = None if full_path == root_path_sep else os.path.sep.join(path.split(os.path.sep)[:-1])
        print(ppath)
        data = {
            'parent_path': ppath,
            'requested_path': path,
            'folders': folders,
            'files': files,
        }
        return render_template('browse.html', data=data)
    else:
        return render_template('404.html'), 404

@app.route('/download')
def download():
    global ROOT_PATH
    path = request.args['path']
    full_path = os.path.abspath(os.path.join(ROOT_PATH, path))
    if full_path.startswith(ROOT_PATH+os.path.sep) and os.path.exists(full_path) and os.path.isfile(full_path):
        parent = os.path.dirname(full_path)
        fn = os.path.basename(full_path)
        return send_from_directory(parent, fn, as_attachment=True)
    else:
        return render_template('404.html'), 404

if __name__ == "__main__":
    app.run(debug=True)